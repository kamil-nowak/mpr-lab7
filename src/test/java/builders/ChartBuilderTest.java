package test.java.builders;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import main.java.builders.ChartBuilder;
import main.java.models.*;

public class ChartBuilderTest {

	@Test
	public void addSerie_should_add_chartSerie_to_list() {
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSerie chartSerie1 = new ChartSerie("label1", Arrays.asList(
				new Point(1, 2), new Point(3, 4)), SerieType.Point);
		ChartSerie chartSerie2 = new ChartSerie("label2", Arrays.asList(
				new Point(5, 6), new Point(7, 8)), SerieType.Point);
		ChartSerie chartSerie3 = new ChartSerie("label3", Arrays.asList(
				new Point(9, 10), new Point(11, 12)), SerieType.Bar);

		ChartSettings chartSettings = chartBuilder.addSerie(chartSerie1)
				.addSerie(chartSerie2).addSerie(chartSerie3).build();

		assertThat(chartSettings.getSeries()).hasSize(3);
		assertThat(chartSettings.getSeries()).contains(chartSerie1).contains(
				chartSerie2);
	}

	@Test
	public void withSeries_should_set_chartSeries() {
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSerie chartSerie1 = new ChartSerie("abcd", Arrays.asList(
				new Point(12, 5), new Point(26, 17), new Point(75, 26)),
				SerieType.LinePoint);
		ChartSettings chartSettings = chartBuilder.withSeries(
				Collections.singletonList(chartSerie1)).build();

		assertThat(chartSettings.getSeries()).hasSize(1);
		assertThat(chartSettings.getSeries()).contains(chartSerie1);
	}

	@Test
	public void withTitle_should_setTitle() {
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "Title";
		ChartSettings chartSettings = chartBuilder.withTitle(title).build();

		assertThat(chartSettings.getTitle()).isEqualTo(title);
	}

	@Test
	public void withSubtitle_should_setSubtitle() {
		ChartBuilder chartBuilder = new ChartBuilder();
		String subtitle = "Subtitle";
		ChartSettings chartSettings = chartBuilder.withSubtitle(subtitle)
				.build();

		assertThat(chartSettings.getSubtitle()).isEqualTo(subtitle);
	}

	@Test
	public void withLegend_should_set_haveLegend_to_true() {
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSettings chartSettings = chartBuilder.withLegend().build();

		assertThat(chartSettings.isHaveLegend()).isTrue();
	}

	@Test
	public void withType_should_SetType() {
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartType testChartType = ChartType.Line;
		ChartSettings chartSettings = chartBuilder.withType(testChartType)
				.build();

		assertThat(chartSettings.getChartType()).isEqualTo(testChartType);
	}

	@Test
	public void should_return_correct_chartSettings() {
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "Title";
		String subtitle = "Subtitle";
		ChartType chartType = ChartType.Bar;
		ChartSettings chartSettings = chartBuilder.withTitle(title)
				.withSubtitle(subtitle).withType(chartType)
				.withLegend().build();

		assertThat(chartSettings.getTitle()).isEqualTo(title);
		assertThat(chartSettings.getSubtitle()).isEqualTo(subtitle);
		assertThat(chartSettings.getChartType()).isEqualTo(chartType);
		assertThat(chartSettings.isHaveLegend()).isTrue();
	}

	@Test
	public void should_return_chartSettings_with_haveLegend_set_as_false_when_with_legend_not_used() {
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "Title";
		String subtitle = "Subtitle";
		ChartSettings chartSettings = chartBuilder.withTitle(title)
				.withSubtitle(subtitle).build();

		assertThat(chartSettings.isHaveLegend()).isFalse();
	}
}
