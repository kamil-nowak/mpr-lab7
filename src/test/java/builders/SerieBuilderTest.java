package test.java.builders;

import main.java.builders.SerieBuilder;
import main.java.models.*;

import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class SerieBuilderTest {

	@Test
	public void addPoint_should_add_point_to_list() {
		SerieBuilder serieBuilder = new SerieBuilder();
		Point point1 = new Point(1, 2);
		Point point2 = new Point(3, 4);
		Point point3 = new Point(5, 6);
		ChartSerie chartSerie = serieBuilder.addPoint(point1).addPoint(point2)
				.addPoint(point3).build();

		assertThat(chartSerie.getPoints()).hasSize(3);
		assertThat(chartSerie.getPoints()).contains(point1).contains(point2)
				.contains(point3);
	}

	@Test
	public void withPoints_should_setPoints() {
		SerieBuilder serieBuilder = new SerieBuilder();
		Point point1 = new Point(66, 55);
		ChartSerie chartSerie = serieBuilder.withPoints(
				Collections.singletonList(point1)).build();

		assertThat(chartSerie.getPoints()).hasSize(1);
		assertThat(chartSerie.getPoints()).contains(point1);
	}

	@Test
	public void withLabel_should_SetTitle() {
		SerieBuilder serieBuilder = new SerieBuilder();
		String label = "Label";
		ChartSerie chartSerie = serieBuilder.withLabel(label).build();

		assertThat(chartSerie.getLabel()).isEqualTo(label);
	}

	@Test
	public void withType_should_setType() {
		SerieBuilder serieBuilder = new SerieBuilder();
		SerieType serieType = SerieType.Bar;
		ChartSerie chartSerie = serieBuilder.withType(serieType).build();

		assertThat(chartSerie.getSerieType()).isEqualTo(serieType);
	}

	@Test
	public void build_should_return_correct_chartSerie() {
		SerieBuilder serieBuilder = new SerieBuilder();
		String label = "Label";
		SerieType serieType = SerieType.Area;
		ChartSerie chartSerie = serieBuilder.withLabel(label)
				.withType(serieType).build();

		assertThat(chartSerie.getLabel()).isEqualTo(label);
		assertThat(chartSerie.getSerieType()).isEqualTo(serieType);
	}
}
