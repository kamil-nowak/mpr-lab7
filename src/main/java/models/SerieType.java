package main.java.models;

public enum SerieType {
    Line, Point, LinePoint, Bar, Area
}
