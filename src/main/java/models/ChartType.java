package main.java.models;

public enum ChartType {
    Line, Point, LinePoint, Bar, StackedBar, Area, Pie
}
